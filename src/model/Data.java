package model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

public class Data {
	
	private static final String FLIGHTS_PATH = "flights.txt";
	private static final String USERS_PATH = "users.txt";
	private static final String RESERVATIONS_PATH = "reservations.txt";
	private static final String TRAVELERS_PATH = "travelers.txt";
	
	private File rootFolder;
	private File flightFile;
	private File usersFile;
	private File reservationsFile;
	private File travelerFile;
	
	private Map<Socket, String> loggedUsers = new HashMap<>();
	private Set<String> users;
	private Set<Flight> flights;
	private Set<Traveler> travelers;
	private Map<Traveler, List<String>> reservations;

	public Data(String dataPath) {
		rootFolder = new File(dataPath);
		if (!rootFolder.exists()) {
			rootFolder.mkdir();
		}
		
		flightFile = new File(rootFolder.getAbsolutePath() + "/" + FLIGHTS_PATH);
		usersFile = new File(rootFolder.getAbsolutePath() + "/" + USERS_PATH);
		reservationsFile = new File(rootFolder.getAbsolutePath() + "/" + RESERVATIONS_PATH);
		travelerFile = new File(rootFolder.getAbsolutePath() + "/" + TRAVELERS_PATH);
		travelers = new HashSet<>();
		
		loadFlights();
		loadUsers();
		loadReservations();
		loadTravelers();
	}

	public void addLoggedUser(Socket client, String username) {
		loggedUsers.put(client, username);
	}
	
	public void logoffUser(Socket client) {
		loggedUsers.remove(client);
	}
	
	public String isLogged(Socket client) {
		return loggedUsers.get(client);
	}

	public boolean containsUser(String username) {
		return users.contains(username);
	}

	public void registerUser(String username) {
		users.add(username);
		saveUsers();
	}

	public void addFlight(Flight flight) {
		flights.add(flight);
		saveFlights();
	}
	
	public void saveFlights() {
		PrintWriter out = null;
		try {
			out = new PrintWriter(flightFile);
			
			out.write(flights.size() + "\n");
			for (Flight f : flights) {
				out.write(f.toString() + "\n");
			}
			
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void saveUsers() {
		PrintWriter out = null;
		try {
			out = new PrintWriter(usersFile);
			
			out.write(users.size() + "\n");
			for (String s : users) {
				out.write(s + "\n");
			}
			
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void loadFlights() {
		flights = new HashSet<>();
		if (!flightFile.exists()) {
			return;
		}
		
		try {
			Scanner sc = new Scanner(flightFile);
			
			int numberOfFlights = Integer.parseInt(sc.nextLine());
			for (int i = 0; i < numberOfFlights; i++) {
				String flightString = sc.nextLine();
				flights.add(new Flight(flightString));
			}
			
			sc.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void loadUsers() {
		users = new HashSet<>();
		if (!usersFile.exists()) {
			return;
		}
		
		try {
			Scanner sc = new Scanner(usersFile);
			
			int numberOfUsernames = Integer.parseInt(sc.nextLine());
			for (int i = 0; i < numberOfUsernames; i++) {
				String username = sc.nextLine();
				users.add(username);
			}
			
			sc.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Flight getFlight(String flightId) {
		for (Flight f : flights) {
			if (f.getId().equals(flightId)) {
				return f;
			}
		}
		return null;
	}
	
	public Collection<Flight> getAllFlights() {
		return flights;
	}

	public String reserveFlight(String flightId, Traveler t) {
		if (reservations.get(t) == null) {
			reservations.put(t, new ArrayList<String>());
		}
		
		if (reservations.get(t).contains(flightId)) {
			return "STATUS=1{~}Flight{~~}is{~~}already{~~}reserved{~~}by{~~}this{~~}traveler";
		}
		
		reservations.get(t).add(flightId);
		saveReservations();
		addTraveler(t);
		return "STATUS=0{~}Flight{~~}is{~~}booked";
	}
	
	public Collection<Traveler> getReservations(String flightId) {
		List<Traveler> travelers = new ArrayList<>();
		for (Entry<Traveler, List<String>> entry : reservations.entrySet()) {
			if (entry.getValue().contains(flightId)) {
				travelers.add(entry.getKey());
			}
		}
		return travelers;
	}
	
	public void saveReservations() {
		PrintWriter out = null;
		try {
			out = new PrintWriter(reservationsFile);
			
			out.write(reservations.size() + "\n");
			for (Entry<Traveler, List<String>> e : reservations.entrySet()) {
				out.write(e.getValue().size() + "\n");
				out.write(e.getKey().toString() + "\n");
				
				for (String flightId : e.getValue()) {
					out.write(flightId + "\n");
				}
			}
			
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void loadReservations() {
		reservations = new HashMap<>();
		if (!reservationsFile.exists()) {
			return;
		}
		
		try {
			Scanner sc = new Scanner(reservationsFile);
			
			int numberOfReservations = Integer.parseInt(sc.nextLine());
			for (int i = 0; i < numberOfReservations; i++) {
				int numberOfFlights = Integer.parseInt(sc.nextLine());
				String travelerString = sc.nextLine();
				Traveler t = new Traveler(travelerString);
				
				List<String> list = new ArrayList<>();
				for (int j = 0; j < numberOfFlights; j++) {
					list.add(sc.nextLine());
				}
				reservations.put(t, list);
			}
			
			sc.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String cancel(String flightId, Traveler t, Data data) {
		List<String> list = data.getReservations(t);
		if (list == null) {
			return "This{~~}traveler{~~}hasn't{~~}reserved{~~}any{~~}flights";
		}
		
		if (!list.contains(flightId)) {
			return "This{~~}traveler{~~}hasn't{~~}reserved{~~}flight: " + flightId;
		}
		
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).equals(flightId)) {
				list.remove(i);
				return "The{~~}flight{~~}has{~~}been{~~}canceled";
			}
		}
		
		saveReservations();
		return "Flight{~~}is{~~}canceled{~~}successfully";
	}

	public List<String> getReservations(Traveler t) {
		return reservations.get(t);
	}
	
	private void addTraveler(Traveler t) {
		if (!travelers.contains(t)) {
			travelers.add(t);
			saveTravelers();
		}
	}

	public Traveler getTraveler(String jmbg) {
		for (Traveler t : travelers) {
			if (t.getJmbg().equals(jmbg)) {
				return t;
			}
		}
		return null;
	}
	
	public void saveTravelers() {
		PrintWriter out = null;
		try {
			out = new PrintWriter(travelerFile);
			
			out.write(travelers.size() + "\n");
			for (Traveler t : travelers) {
				out.write(t.toString() + "\n");
			}
			
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void loadTravelers() {
		travelers = new HashSet<>();
		if (!travelerFile.exists()) {
			return;
		}
		
		try {
			Scanner sc = new Scanner(travelerFile);
			
			int numberOfUsernames = Integer.parseInt(sc.nextLine());
			for (int i = 0; i < numberOfUsernames; i++) {
				String travelerString = sc.nextLine();
				travelers.add(new Traveler(travelerString));
			}
			
			sc.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
