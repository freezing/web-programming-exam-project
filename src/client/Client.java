package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client implements Runnable {
	private static String IP_ADDRESS = "127.0.0.1";
	private static int PORT = 12345;
	
	private Socket socket;
	BufferedReader in;
	PrintWriter out;
	Scanner sc;
	
	public Client() {
		init();
	}
	
	private void init() {
		try {
			sc = new Scanner(System.in);
			socket = new Socket(InetAddress.getByName(IP_ADDRESS), PORT);
			in = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			out = new PrintWriter(new OutputStreamWriter(
					socket.getOutputStream()), true);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		
		while (true) {
			String lineToSend = sc.nextLine();
			out.println(lineToSend);
			System.out.println("Sent to server: " + lineToSend);
			
			String resultLine = null;
			try {
				System.out.println("\nReading from server...");
				while (true) {
					resultLine = in.readLine();
					System.out.println(resultLine);
					if (resultLine.equalsIgnoreCase("end")) {
						break;
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) {
		new Client().run();
	}
}
