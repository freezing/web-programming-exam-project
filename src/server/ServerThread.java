package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import commands.ServerCommand;

public class ServerThread extends Thread {
	private Socket client;

	private BufferedReader in;
	private PrintWriter out;

	public ServerThread(Socket client) {
		this.client = client;
		initialize();
	}

	private void initialize() {
		try {
			in = new BufferedReader(new InputStreamReader(
					client.getInputStream()));
			out = new PrintWriter(new OutputStreamWriter(
					client.getOutputStream()), true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			String line = "";
			
			while (true) {
				System.out.println("Waiting for request from client...");
				line = in.readLine();
				System.out.println("Client requsted: " + line);
				
				if (line.equalsIgnoreCase("quit") || line.equalsIgnoreCase("exit")) {
					break;
				}
				
				ServerCommand command = new ServerCommand(line);
				String response = command.doAction(client);
				
				System.out.println("Sending response to client:\n" + response);
				out.println("START");
				out.println(response);
				out.println("END");
				System.out.println("Response has been sent to client");
			}

			in.close();
			out.close();
			client.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
