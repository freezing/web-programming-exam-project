package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import model.Data;

public class Server {
	private static final String DATA_PATH = "D:/zad1_data/";
	public static final int TCP_PORT = 12345;
	
	private static Server instance = null;
	
	public static Server getInstance() {
		if (instance == null) {
			instance = new Server();
			instance.data = new Data(DATA_PATH);
		}
		return instance;
	}
	
	private Data data;

	public static void main(String[] args) {
		
		try {
			@SuppressWarnings("resource")
			ServerSocket ss = new ServerSocket(TCP_PORT);
			System.out.println("Server running...");
			
			while (true) {
				Socket client = ss.accept();
				new ServerThread(client).start();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Data getData() {
		return data;
	}
}
