package server;

import java.net.Socket;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import model.Data;
import model.Flight;
import model.Traveler;

public class Utils {
	public static String login(String[] args, Data data, Socket client) {
		if (args == null || args.length != 1) {
			return "STATUS=1{~}"
				+ "Command \"LOGIN\" must have exactly one parameter!{~}"
				+ "If you want to use spaces use quotes \"<parameter>\"";
		}

		if (data.containsUser(args[0])) {
			data.addLoggedUser(client, args[0]);
			return "STATUS=0{~}"
					+ "Successfully logged in";
		} else {
			return "STATUS=2{~}"
					+ "Given username \"" + args[0]
					+ "\" doesn't exist! Please register.";
		}
	}

	public static String register(String[] args, Data data) {
		if (args.length != 1) {
			return "STATUS=1{~}"
					+ "Command \"REGISTER\" must have exactly one parameter!{~}"
					+ "If you want to use spaces use quotes \"<parameter>\"";
		}

		if (!data.containsUser(args[0])) {
			data.registerUser(args[0]);
			return "STATUS=0{~}"
					+ "Successfully registered";
		} else {
			return "STATUS=2{~}"
					+ "Username \"" + args[0] + "\" already exists!";
		}
	}

	public static String logoff(Data data, Socket client) {
		if (data.isLogged(client) != null) {
			data.logoffUser(client);
			return "STATUS=0{~}"
					+ "Successfully logged off!";
		}
		return "STATUS=1{~}"
				+ "You are not logged in!";
	}

	public static String add(String[] args, Data data) {
		String flightString = Utils.joinString(args, "{~~}");
		data.addFlight(new Flight(flightString));
		return "STATUS=0{~}"
				+ "Successfully added flight";
	}

	public static String list(String[] args, Data data) {
		String flightId = args[0];
		Flight f = data.getFlight(flightId);
		if (f == null) {
			return "STATUS=1{~}"
					+ "Unknown flight id";
		}
		return "STATUS=0{~}"
				+ f.toString();
	}
	
	public static String listReservations(String[] args, Data data) {
		String flightId = args[0];
		String s = "STATUS=0";
		for (Traveler t : data.getReservations(flightId)) {
			s += "{~}" + t.toString();
		}
		return s;
	}
	
	public static String listPassengerReservations(String[] args, Data data) {
		String jmbg = args[0];
		String s = "STATUS=0";
		List<String> flightIds = data.getReservations(new Traveler(jmbg, null, null));
		Collection<Flight> flights = data.getAllFlights();
		
		for (Flight f : flights) {
			if (flightIds.contains(f.getId())) {
				s += "{~}" + f.toString();
			}
		}
		return s;
	}

	public static String listAll(Data data) {
		String s = "STATUS=0";
		for (Flight f : data.getAllFlights()) {
			s += "{~}" + f.toString();
		}
		return s;
	}

	public static String reserve(String[] args, Data data) {
		String flightId = args[0];
		Traveler t = new Traveler(args[1], args[2], args[3]);
		return data.reserveFlight(flightId, t);
	}

	public static String cancel(String[] args, Data data) {
		String flightId = args[0];
		Traveler t = Traveler.fromId(args[1]);
		return "STATUS=0{~}" + data.cancel(flightId, t, data);
	}

	public static String listPassenger(String[] args, Data data) {
		String jmbg = args[0];
		Traveler t = data.getTraveler(jmbg);
		if (t == null) {
			return "STATUS=1{~}"
					+ "Traveler{~~}doesn't{~~}exist";
		}
		return "STATUS=0{~}"
				+ t.toString();
	}

	public static String[] splitString(String line) {
		return line.split("\\{~\\}");
	}
	
	public static String joinString(String[] args, String joiner) {
		String result = "";
		for (int i = 0; i < args.length; i++) {
			result += args[i];
			if (i < args.length - 1) {
				result += joiner;
			}
		}
		return result;
	}

	public static Date getDate(String dateString) {
		String[] tmp = dateString.split("-");
		for (int i = 0; i < tmp.length; i++) {
			tmp[i] = tmp[i].trim();
		}

		int year = Integer.parseInt(tmp[0]);
		int month = Integer.parseInt(tmp[1]);
		int day = Integer.parseInt(tmp[2]);

		int hour = Integer.parseInt(tmp[3].split(":")[0]);
		int minute = Integer.parseInt(tmp[3].split(":")[1]);
		return new Date(year, month, day, hour, minute);
	}

	public static String dateToString(Date date) {
		return date.getYear() + "-" + date.getMonth() + "-" + date.getDay()
				+ "-" + date.getHours() + ":" + date.getMinutes();
	}

	public static String[] splitStringSpaces(String s) {
		return s.split("\\{~~\\}");
	}
}
