package commands;

import java.net.Socket;
import java.util.Arrays;

import model.Data;
import server.Server;
import server.Utils;

public class ServerCommand {
	private ServerCommandEnum type;
	private String[] args;
	
	public ServerCommand(String line) {
		System.out.println("Debug: " + line);
		type = parseCommand(line);
	}
	
	public synchronized String doAction(Socket client) {	
		System.out.println("Doing an action...");
		Data data = Server.getInstance().getData();
		switch (type) {
		case ADD:
			return Utils.add(args, data);
		case LOGIN:
			return Utils.login(args, data, client);
		case REGISTER:
			return Utils.register(args, data);
		case LOGOFF:
			return Utils.logoff(data, client);
		case LIST:
			return Utils.list(args, data);
		case LIST_ALL:
			return Utils.listAll(data);
		case LIST_RESERVATIONS:
			return Utils.listReservations(args, data);
		case RESERVE:
			return Utils.reserve(args, data);
		case CANCEL:
			return Utils.cancel(args, data);
		case LIST_PASSENGER:
			return Utils.listPassenger(args, data);
		case LIST_PASSENGER_RESERVATIONS:
			return Utils.listPassengerReservations(args, data);
		default:
			break;
		}
		return "Unknown command!";
	}
	
	ServerCommandEnum parseCommand(String line) {
		String args[] = Utils.splitString(line);
		if (args.length == 0) {
			return ServerCommandEnum.UNKNOWN;
		}
		
		if (args.length > 1) {
			this.args = Arrays.copyOfRange(args, 1, args.length);
		}
		
		for (ServerCommandEnum cmd : ServerCommandEnum.values()) {
			if (cmd.toString().equals(args[0])) {
				return cmd;
			}
		}
		return ServerCommandEnum.UNKNOWN;
	}
}
