package commands;

public enum ServerCommandEnum {
	LOGIN("LOGIN"),
	REGISTER("REGISTER"),
	LOGOFF("LOGOFF"),
	ADD("ADD"),
	LIST("LIST"),
	LIST_ALL("LIST_ALL"),
	RESERVE("RESERVE"),
	CANCEL("CANCEL"),
	LIST_PASSENGER("LIST_PASSENGER"),
	LIST_RESERVATIONS("LIST_RESERVATIONS"),
	LIST_PASSENGER_RESERVATIONS("LIST_PASSENGER_RESERVATIONS"),
	UNKNOWN("Unknown command");
	
	private String value;
	
	ServerCommandEnum(String value) {
		this.value = value;
	}
	
	public String toString() {
		return value;
	}
}
